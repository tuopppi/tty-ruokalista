module.exports = {
	
	labelStyle: {
		display: 'inline-block',
		margin: '0 .2em',
		padding: '.4em',
		fontSize: '80%',
		backgroundColor: '#eee',
		borderRadius: '5px'
	}

}