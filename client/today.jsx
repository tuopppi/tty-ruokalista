var React = require('react');
var moment = require('moment');

module.exports.Today = React.createClass({
  render: function() {
    var weekdayStyle = {
      textTransform: 'capitalize'
    };
    return (
      <h1 style={weekdayStyle}>
        {moment(this.props.date).format("dddd")} {moment(this.props.date).format("D. MMMM")}ta
      </h1>
    );
  }
});
