var React = require('react');
var _ = require('underscore');

var styles = require('./styles.js');

function setCookie(cname, cvalue) {
    var d = new Date();
    d.setTime(d.getTime() + (365*24*60*60*1000));
    var expires = "expires="+d.toUTCString();
    document.cookie = cname + "=" + cvalue + "; " + expires + "; path=/";
    document.location="/";
}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i=0; i<ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1);
        if (c.indexOf(name) == 0) return c.substring(name.length,c.length);
    }
    return "";
}

function clickableLabelStyle(value, selected) {
  var hl = value == selected ? 'black' : 'gray';
  return _.extend({ 
    cursor: 'pointer', 
    color: hl,
    border: '1px solid ' + hl
  }, styles.labelStyle);
}

var PriceLink = React.createClass({
  cookieName: "filters_maxPrice",
  setMaxPrice: function(event) {
    setCookie(this.cookieName, this.props.price);
  },
  render: function() {
    return (<li style={clickableLabelStyle(this.props.price, getCookie(this.cookieName))} 
                onClick={this.setMaxPrice} 
                data-price={this.props.price}>
                {this.props.text}
            </li>);
  }
});

var CategoryLink = React.createClass({
  cookieName: "filters_category",
  setCategory: function(event) {
    setCookie(this.cookieName, this.props.category);
  },
  render: function() {
    return (<li style={clickableLabelStyle(this.props.category, getCookie(this.cookieName))} 
                onClick={this.setCategory}>
              {this.props.children}
            </li>);
  }
});

module.exports = React.createClass({
  listStyle: {
    marginTop: '.5em'
  },
  footerTextStyle: {
    marginTop: '2em'
  },
  render: function() {
    return (
      <footer>
        <h4>Filtterit</h4>
        <ul style={this.listStyle}>
          <PriceLink price={1.50} text="1.5€" />
          <PriceLink price={2.60} text="2.6€" />
          <PriceLink price={100.0} text="Kaikenhintaiset" />
        </ul>
        <ul style={this.listStyle}>
          <CategoryLink category="student">Opiskelija</CategoryLink>
          <CategoryLink category="staff">Henkilökunta</CategoryLink>
          <CategoryLink category="other">Muu</CategoryLink>
        </ul>
        <div style={this.footerTextStyle}>
          Tämä on kolmannen osapuolen tarjoama palvelu. 
          En voi taata ruokalistojen oikeellisuutta.
        </div>
      </footer>
    );
  }
});