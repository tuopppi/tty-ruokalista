var React = require('react');
var $ = require('jquery');
var styles = require('./styles.js');

var FoodOption = React.createClass({
  render: function() {
    var itemStyle = {
      margin: '1em 0'
    };
    var sideStyle = {
      margin: '.5em'
    };
    var setStyle = {
      display: 'inline-block'
    };
    var pricesStyle = {
      display: 'inline-block',
      marginLeft: '.5em'
    };
    return (
      <li style={itemStyle}>
        <h3 style={setStyle}>{this.props.main.Name}</h3>
        <div style={pricesStyle}>
          {this.props.prices.map(function(price) {
            return (<span style={styles.labelStyle}>{price}€</span>);
          })}
        </div>
        <ul style={sideStyle}>
          {this.props.sides.map(function(side, idx) {
            return (<li key={idx}>{side.Name}</li>);
          })}
        </ul>
      </li>
    );
  }
});

var KitchenMenu = React.createClass({
  render: function() {
    var containerStyle = {
      margin: '2em 0 1em 0'
    };
    var menuStyle = {
      borderLeft: '.4em solid #ccc',
      margin: '1em 0 2em .2em',
      paddingLeft: '.8em',
      borderColor: this.props.color
    };
    return (
      <div style={containerStyle}>
        <h2>{this.props.name}</h2>
        <ul style={menuStyle}>
          {this.props.options.map(function(option, idx) {
            return <FoodOption key={idx} main={option.Main} sides={option.Sides} prices={option.Prices} />;
          })}
        </ul>
      </div>
    );
  }
});

module.exports.KitchenMenuContainer = React.createClass({
  getInitialState: function() {
    return {
      items: [],
      error: null,
      loaded: false
    };
  },

  getDefaultProps: function() {
    return {
      colors: ['#A3CF62','#FAAA4A','#84D6F1']
    };
  },

  componentDidMount: function() {
    $.getJSON(this.props.source)
      .done(function(result)
      {
        this.setState({ items: result, error: null, loaded: true });
      }.bind(this))
      .fail(function(jqXHR, textStatus, errorThrown)
      {
        this.setState({ items: [], error: (jqXHR.status + ' ' + jqXHR.statusText), loaded: true });
      }.bind(this));
  },

  render: function() {
    var colors = this.props.colors;

    if(this.state.error) {
      return (<div>Virhe ladattaessa ruokalistoja ({this.state.error})</div>);
    }

    if(this.state.loaded)
    {
      if(this.state.items.length > 0) {
        return (
          <div className='kitchenMenuContainer'>
            {this.state.items.map(function(item, idx) {
              return <KitchenMenu key={idx} color={colors[idx]} name={item.CustomKitchenName} options={item.Options} />;
            })}
          </div>
        );
      }
      else {
        return (
          <div>Tänään ei saa ruokaa mistään :(</div>
        );
      }
    }
    else {
      return <div>Ladataan...</div>
    }
  }
});
