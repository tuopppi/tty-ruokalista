var React = require('react');
var moment = require('moment');

require('moment/locale/fi.js');
moment.locale("fi");

var Today = require('./today.js').Today;
var KitchenMenuContainer = require('./menus.js').KitchenMenuContainer;
var Footer = require('./footer.js');

React.render(
	<div>
		<Today date={new Date()} />
		<KitchenMenuContainer source="/api/kitchens" />
    	<Footer />
	</div>,
	document.getElementById('main')
);