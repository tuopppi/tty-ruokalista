FROM node
MAINTAINER Tuomas Vaherkoski <tuomasvaherkoski@gmail.com>
EXPOSE 3000
ENV PORT=3000 IP=0.0.0.0 NODE_ENV=production
WORKDIR /app
CMD npm start
COPY ./ ./
RUN npm install --no-dev --no-optional
