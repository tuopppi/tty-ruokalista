var Promise = require('bluebird');
var _ = require('underscore');
var readFile = Promise.promisify(require("fs").readFile);
var realLoader = require('../server/loader.js');

var urlMap = {
  ".*juvenes.*MenuTypeId=3.*": { filename: "konehuone_fusion.jsonp" },
  ".*juvenes.*MenuTypeId=60": { filename: "newton.jsonp" },
  ".*juvenes.*MenuTypeId=74": { filename: "newton_extra.jsonp" },
  ".*juvenes.*MenuTypeId=77": { filename: "konehuone_såås.jsonp" },
  ".*amica.*": { 
    filename: "reaktori.json",
    modify: data => { 
      var json = JSON.parse(data);
      json.MenusForDays[0].Date = new Date().toJSON();
      return JSON.stringify(json);
    }
  },
  ".*sodexo.*": { filename: "sodexo.json" }
};

function testResponse(url)
{
  var info;
  for(var matcher in urlMap)
  {
    var match = new RegExp(matcher).test(url);
    if(match)
    {
      info = urlMap[matcher];
      break;
    }
  }

  if(info.filename)
  {
    return readFile(__dirname + "/" + info.filename, "utf8")
      .then(data => {
        if(info.modify !== undefined) return info.modify(data);
        else return data;
      })
      .then(function(data) {
        return {
          url: url,
          body: data,
          statusCode: 200
        };
      });
  }
  else
  {
    return Promise.reject({ error: "No matching file for url " + url });
  }
}

exports.loadData = function(url) {
  return testResponse(url);
};

exports.getCachedMenu = realLoader.getCachedMenu;

exports.cacheMenu = realLoader.cacheMenu;
