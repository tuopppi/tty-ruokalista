var _ = require('underscore');
var moment = require('moment');
var entities = require('html-entities'); ;

function decodeHtmlEntities(text)
{
  return new entities.AllHtmlEntities().decode(text);
}

function parsePrices(priceData)
{
  return _.map(priceData.split('/'), str => parseFloat(str.replace(',','.').trim()));
}

function parse(kitchen, data) {
  var json = JSON.parse(data);

  var options = _
    .chain(json.courses)
    .sortBy("category")
    .map(function(set) {
      return {
        Prices: parsePrices(set.price),
        Main: {
          Name: set.category,
          Diets: set.properties
        },
        Sides: [
          { Name: decodeHtmlEntities(set.title_fi) },
          { Name: decodeHtmlEntities(set.desc_fi) }
        ]
      };
    })
    .value();

  return {
    "Date": new Date(),
    "KitchenName": kitchen,
    "CustomKitchenName": kitchen,
    "Options": options
  };
}

exports.availableKitchens = function() { return ["Hertsi"]; };

exports.tryToParse = function(kitchen, data) { return parse(kitchen, data); }

exports.url = function(kitchen) {
  return 'http://www.sodexo.fi/ruokalistat/output/daily_json/12812/' +
            moment().format('YYYY/MM/DD') + '/fi';
};
