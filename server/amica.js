var _ = require('underscore');

function combineDiets(components)
{
  // Diets are listed at the end of dishes as comma separated list in brackets
  // eg. "Dish name (V, GL)"
  return _
    .chain()
    .map(function(nameWithDiets) { return /\((.*)\)$/.exec(nameWithDiets)[1].split(","); })
    .flatten()
    .map(function(diet) { return diet.trim(); })
    .unique()
    .value()
    .join();
}

function getDishName(nameWithDiets)
{
  return { Name: /^(.+) \(.*\)$/.exec(nameWithDiets)[1] };
}

/**
 * Parce price data formatted as "2,60/4,87/7,50€"
 */
function parsePrices(priceData)
{
  var cleanup = str => str.trim().replace(/€$/, "").replace(',','.');
  return _.map(priceData.split('/'), str => parseFloat(cleanup(str)));
}

function parse(data) {
  var json = JSON.parse(data);

  // On Sundays Amica API will report Saturdays menu as "todays menu" at index 0
  var amicamenu = _.find(json.MenusForDays, isTodaysMenu);
  var options =
   _.chain(amicamenu.SetMenus)
    .map(function(set) {
      return {
        Prices: parsePrices(set.Price),
        Main: {
          Name: set.Name, // "Linjasto, Bistro, Leipäateria, ..."
          Diets: combineDiets(set.Components)
        },
        Sides: _.map(set.Components, getDishName)
      };
    })
    .value();

  return {
    "Date": amicamenu.Date,
    "KitchenName": json.RestaurantName,
    "CustomKitchenName": json.RestaurantName,
    "Options": options
  };
}

function isTodaysMenu(menu)
{
  return new Date(menu.Date).getDay() == today();
}

function today()
{
  return new Date().getDay();
}

exports.availableKitchens = function() { return ["Reaktori"]; };

exports.tryToParse = function(kitchen, data) { return parse(data); };

exports.url = function(kitchen) {
  return 'http://www.amica.fi/modules/json/json/Index?costNumber=0812&language=fi';
};
