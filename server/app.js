var express = require('express');
var Promise = require('bluebird');
var morgan = require('morgan');
var _ = require('underscore');
var cookieParser = require('cookie-parser');

var logger = require('./logger.js');
var filter = require('./filter.js');
var juvenes = require('./juvenes.js');
var amica = require('./amica.js');
var sodexo = require('./sodexo.js');
var loader = process.env.NODE_ENV === "test"  
           ? require('../test/loader.js')  
           : require('./loader.js');

// -----------------------------------------------------------------------------

var app = express();

app.set('json spaces', 2);

var logFormat = process.env.NODE_ENV === "test" ? 'dev' : 'common';
app.use(morgan(logFormat, { "stream": logger.stream }));
app.use(express.static(__dirname + '/static'));
app.use(cookieParser())

var server = app.listen(
  process.env.PORT || 3000,
  process.env.IP || '0.0.0.0',
  () => { console.log('Listening %j %s', server.address(), process.env.NODE_ENV); }
);

// -----------------------------------------------------------------------------

var cookieSettings = { maxAge: 900000 };

var filters = (req, res) => {
  var filters = { 
    maxPrice: parseFloat(req.cookies.filters_maxPrice) || 2.60, 
    category: req.cookies.filters_category || 'student'
  };
  res.cookie('filters_maxPrice', filters.maxPrice, cookieSettings);
  res.cookie('filters_category', filters.category, cookieSettings);
  return filters;
}

app.get('/api/kitchens', (req, res) => { 
  var activeFilters = filters(req,res);
  loader.getCachedMenu(activeFilters)
        .then(cacheValue => {
          if(cacheValue !== undefined) {
            logger.info('Hit cache with filters ' + JSON.stringify(activeFilters));
            return res.json(cacheValue);
          }
          else {
            return loadDataAndSaveToCache(activeFilters, res);
          }
        });
});

function loadDataAndSaveToCache(filters, res)
{
  var optionFilter = _.partial(filter.by, filters);
  
  var loaders =
   _.chain([amica, juvenes, sodexo])
    .map(company => {
      return _.map(company.availableKitchens(), kitchen => {
        return {
          url: company.url(kitchen),
          parser: _.partial(company.tryToParse, kitchen)
        };
      });
    })
    .flatten()
    .map(kitchen => 
      loader.loadData(kitchen.url)
            .then(response => kitchen.parser(response.body))
            .then(optionFilter)                       
    )
    .value();

  return Promise
    // Get results from all companies
    .settle(loaders)
    // Log errors
    .map(result => {
      if(result.isRejected()) { console.error(result.reason().stack); } 
      return result; 
    })
    // Remove failed requests and if no food is available today
    .filter(r => r.isFulfilled() && r.value().Options.length > 0)
    // Pick returned values
    .map(result => result.value())
    // Merge returned values into one array
    .all()
    // Merge same restaurants menu's to one entry (Newton and Newton Rohee Extra for example...)
    .then(menus => {
      return _.chain(menus)
              .groupBy("CustomKitchenName")
              .map(group => {
                group[0].Options = _.chain(group)
                                    .pluck("Options")
                                    .flatten()
                                    .sortBy(item => item.Main.Name)
                                    .value();
                return group[0];
              })
              .value();
    })
    // Return merged array as JSON
    .then(menu => { res.json(menu); return menu; })
    .then(menu => loader.cacheMenu(filters, menu))
    .catch(error => {
      console.error(error.stack);
      return res.status(error.code || 500)
                .json({ error: error });
    });
}

