var _ = require('underscore');
var querystring = require('querystring');

var parse = function(data) {
  var parsedData = JSON.parse(data.d);
  var menuDate;

  var options = _.map(parsedData.MealOptions, function(option) {
    menuDate = option.MenuDate;

    return {
      Main: { Name: parsedData.MenuTypeName },
      Sides: _.chain(option.MenuItems)
                .filter(function(item) { return item.Name !== ''; })
                .map(function(item) { return { Name: item.Name, Diets: item.Diets }; })
                .value()
    };
  });

  if(options.length > 0 && options[0].Main === undefined)
  {
    options = [];
  }

  return {
    Date: menuDate,
    KitchenName: parsedData.Name.replace(/[\d-]*/gi, ""),
    CustomKitchenName: "",
    Options: options
  };
};

var avail = {
  "Café Konehuone Rohee Xtra": {
    typeId: 74,
    kitchenId: 60038,
    lineName: "Rohee Xtra",
    plainName: "Café Konehuone",
    prices: [4.00, 7.25, 9.90]
  },
  "Café Konehuone Fusion Kitchen": {
    typeId: 3,
    kitchenId: 60038,
    lineName: "Fusion Kitchen",
    plainName: "Café Konehuone",
    prices: [4.95, 7.37, 10.20]
   },
  "Newton": {
    typeId: 60,
    kitchenId: 6,
    lineName: "Linjasto",
    plainName: "Newton",
    prices: [2.60, 4.87, 9.90]
  },
  "Newton Såås Bar": {
    typeId: 77,
    kitchenId: 6,
    lineName: "Såås Bar",
    plainName: "Newton",
    prices: [2.60, 4.87, 9.90]
  }
};

function getWeek() {
  var today = new Date();
  var onejan = new Date(today.getFullYear(),0,1);
  return Math.ceil((((today - onejan) / 86400000) + onejan.getDay()+1)/7);
}

function getJuveDay()
{
  var today = new Date();
  return (today.getDay() + 6) % 7 + 1; // monday = 1 .. sunday = 7
}

function juveUrl(kitchen)
{
  return 'http://www.juvenes.fi/DesktopModules/Talents.LunchMenu/LunchMenuServices.asmx/GetMenuByWeekday?lang=%27fi%27&' +
            querystring.stringify({
              format: "json",
              callback: "parse",
              MenuTypeId: avail[kitchen].typeId,
              KitchenId: avail[kitchen].kitchenId,
              Week: getWeek(),
              Weekday: getJuveDay(),
            });
}

exports.availableKitchens = function() {
  if(getJuveDay() !== 7) {
    return _.keys(avail);
  }
  else {
    // On Sundays Juvenes api will return error 500...
    return [];
  }
};

exports.tryToParse = function(kitchen, body) {
  var menu = eval(body); // Calls parse function
  menu.CustomKitchenName = avail[kitchen].plainName;
  _.each(menu.Options, option => option.Prices = avail[kitchen].prices);
  return menu;
};

exports.url = function(kitchen) { return juveUrl(kitchen); };
