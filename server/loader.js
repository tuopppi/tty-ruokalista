var NodeCache = require('node-cache');
var http = require('http');
var Promise = require('bluebird');

var ttl = 1 * 60 * 60; // 1 hour in seconds
var menuCache = new NodeCache({
  stdTTL: ttl,
  checkperiod: ttl
});

exports.loadData = function(url) {
  return new Promise(function(resolve, reject) {
    http.get(url, function(jsonp_res) {
      var body = '';
      jsonp_res.on('data', function(chunk) { body += chunk; });
      jsonp_res.on('end', function() {
        var code = jsonp_res.statusCode;
        if(code == 200) {
          return handleSuccess(resolve, reject, body, url, code);
        } else {
          return handleFailure(reject, url, code);
        }
      });
    }).on('error', reject);
  });
};

function handleSuccess(resolve, reject, body, url, code)
{
  try
  {
    return resolve({
      url: url,
      body: body,
      statusCode: code
    });
  }
  catch(e) {
    return reject({
      message: e.message,
      url: url,
      exception: e.stack
    });
  }
}

function handleFailure(reject, url, code)
{
  return reject({
    statusCode: code,
    message: url
  });
}

function cacheKey(filters)
{
  return filters.category + "-" + filters.maxPrice;
}

exports.getCachedMenu = function(filters)
{
  return Promise.promisify(menuCache.get)(cacheKey(filters));
};

exports.cacheMenu = function(filters, data)
{
  return Promise.promisify(menuCache.set)(cacheKey(filters), data);
};
