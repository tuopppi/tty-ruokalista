var _ = require('underscore');

/**
 * filters.maxPrice: Maximum price (float)
 * filters.category: one of ['student', 'staff', 'other']
 */
module.exports.by = (filters, menu) => {

	var selectPriceByCategory = option => {
		option.Prices = [option.Prices[categoryToIndex(filters.category)]]
		return option;
	}
	
	var priceFilter = option => filters.maxPrice >= 100 
	                          ? true 
							  : option.Prices[0] === filters.maxPrice;
	
	menu.Options = _.chain(menu.Options)
	                .map(selectPriceByCategory)
	                .filter(priceFilter)
					.value();
	
	return menu;
} 

/** 
 * Convert filters.category to index in menu.Option.Prices array 
 */
function categoryToIndex(category) {
	if(category === 'student') return 0;
	if(category === 'staff') return 1;
	if(category === 'other') return 2;
	
	return -1;	
}