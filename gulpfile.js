var gulp = require('gulp');
var react = require('gulp-react');
var browserify = require('gulp-browserify');
var rename = require('gulp-rename');
var uglify = require('gulp-uglify');
var sourcemaps = require('gulp-sourcemaps');

var scripts = {
  jsx: 'client/**/*.js*',
  jsx_out: './build/jsx/'
};

function destBuild() { 
  return gulp.dest('./build');
}

gulp.task('jsx', function() {
  return gulp.src(scripts.jsx)
             .pipe(react())
             .pipe(gulp.dest(scripts.jsx_out));
});

gulp.task('browserify', ['jsx'], function() {
  return gulp.src(scripts.jsx_out + 'main.js')
             .pipe(browserify())
             .pipe(rename('bundle.js'))
             .pipe(destBuild());
});

gulp.task('uglify', ['jsx', 'browserify'], function() {
  return gulp.src('build/bundle.js')
             .pipe(uglify())
             .pipe(rename('bundle.min.js'))
             .pipe(destBuild());
});

gulp.task('copy', ['browserify'], function() {
  return gulp.src('build/bundle?(.min).js')
             .pipe(gulp.dest('./server/static'));
});

gulp.task('default', ['jsx', 'browserify', 'copy']);

gulp.watch(scripts.jsx, ['browserify', 'copy']);